package StepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class UrlTestSteps {
	

	public WebDriver driver;
	
	@Given("Launch url")	
	public void Launch_url() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		options.addArguments("--no-sandbox");
		options.addArguments("disable-gpu");
		driver = new ChromeDriver(options);
		driver.get("http://54.167.69.43:8080/webapp/");
		
	}
	
@Then("Verify url")
	
	public void Verify_url() {
		
	String test = driver.findElement(By.xpath("/html/body/h1[2]")).getText();
    System.out.println(test);
    driver.close();

	}


}
