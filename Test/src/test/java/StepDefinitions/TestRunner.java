package StepDefinitions;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
features = "src/test/resources/Features/UrlTest.feature",
glue={"StepDefinitions"} ,plugin={"html:target/cucumber-reports/HtmlReports.html",
"json:target/cucumber-reports/CucumberTestReport.json"},
monochrome= true,
dryRun= false
)


public class TestRunner extends AbstractTestNGCucumberTests{

}
